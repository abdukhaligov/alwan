<?php include 'modules/header.php'; ?>

<?php include 'modules/showroommin.php'; ?>

<?php include 'modules/navmap.php'; ?>


    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 column-content">
                    <div class="gslider-wrapper">
                        <div id="gslider" class="flexslider">
                            <ul class="slides">
                                <li>
                                    <img src="images/head1.jpg" alt="" class="img-responsive">
                                </li>
                                <li>
                                    <img src="images/head2.jpg" alt="" class="img-responsive">
                                </li>
                                <li>
                                    <img src="images/head3.jpg" alt="" class="img-responsive">
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div>
                        <div class="title2">
                            <p>Company Introduction :</p>
                        </div>
                        <p>
                            Established in 1990, Alwan Cars LLC has become one of the leading Automotive trading
                            companies in the UAE, which specializes in Brand New Vehicles Re-Exportation. With humble
                            beginnings as a small used cars trading company in Sharjah, it has emerged today as a
                            stable, competitive and trusted company in the Automotive industry reaching the markets as
                            far as Asia, Europe, Middle East, and Africa.
                        </p>
                        <p>
                            The company takes pride in providing our customers the Vehicle of their choice with
                            different brands ranging from Mitsubishi, Toyota, Nissan, Kia, Hyundai and luxury cars such
                            as Range Rover, Bentley, Mercedes Benz, BMW, Lexus, Maybach and Rolls Royce. Commercial
                            Vehicles as Passenger cars, sport utility vehicles, trucks, pickups.. It has also been the
                            company’s pleasure to have acquired the third manufactured Range STORMER in the world.
                        </p>
                        <p>
                            Alwan Cars LLC emphasizes on excellent customer service, good work ethic, professionalism
                            and team work. Customer satisfaction is the highest priority, by providing the clients high
                            quality vehicles at excellent value.
                        </p>
                        <p>
                            As the company grows from strength to strength in line with its vision, In 2009 it has
                            expanded and now operates 6 showrooms in Dubai, 2 Showrooms in Ajman, 2 Showroom in DUCAMZ,
                            1 showroom in Abu Dhabi and 1 in Jebel Ali Free Zone (JAFZA).Today The company reaches 100
                            competitive personnel/employees distributed to 9 departments
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

<?php include 'modules/suggestions.php'; ?>

<?php include 'modules/footer.php'; ?>