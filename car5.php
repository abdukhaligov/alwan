<?php include 'modules/header.php'; ?>

<?php include 'modules/showroommin.php'; ?>

<?php include 'modules/navmap.php'; ?>

    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-9 column-content">
                    <div class="gslider-wrapper">
                        <div id="gslider" class="flexslider">
                            <ul class="slides">
                                <li><img src="images/e1.png" alt="" class="img-responsive"></li>
                                <li><img src="images/e2.png" alt="" class="img-responsive"></li>
                                <li><img src="images/e3.png" alt="" class="img-responsive"></li>
                                <li><img src="images/e4.png" alt="" class="img-responsive"></li>
                            </ul>
                        </div>
                        <div id="carousel" class="flexslider">
                            <ul class="slides">
                                <li><img src="images/e1.png" alt="" class="img-responsive"></li>
                                <li><img src="images/e2.png" alt="" class="img-responsive"></li>
                                <li><img src="images/e3.png" alt="" class="img-responsive"></li>
                                <li><img src="images/e4.png" alt="" class="img-responsive"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tabs2-wrapper">
                        <ul class="tabs clearfix" data-tabgroup="second-tab-group">
                            <li class="active"><a href="#tabs3-1">VEHICLE OVERVIEW</a></li>
                            <li><a href="#tabs3-2">FEATURES & OPTIONS</a></li>
                            <li><a href="#tabs3-3">COMMENTS</a></li>
                        </ul>
                    </div>
                    <div id="second-tab-group" class="tabgroup">
                        <div id="tabs3-1">
                            <div class="title2">
                                <p>DESCRIPTION</p>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="left">
                                        <p>Model</p>
                                        <p>Description</p>
                                        <p>Body Type</p>
                                        <p>Exterior Dimensions (L x W x H)(m m)</p>
                                        <p>Engine</p>
                                        <p>Displacement cc</p>
                                        <p>Max Power hp/rpm</p>
                                        <p>Max Torque kq.m/rpm</p>
                                        <p>Fuel System</p>
                                        <p>Transmission</p>
                                        <p>Streening</p>
                                        <p>Fuel Tank Capacity (litres)</p>
                                        <p>Brakes</p>
                                        <p>Front</p>
                                        <p>Rear</p>
                                        <p>Suspension</p>
                                        <p>Wheels</p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="right">
                                        <p> Nissan Patrol My 2018</p>
                                        <p>Patrol Le VK56 V8 7AT PTNM P18</p>
                                        <p>SUV</p>
                                        <p>5140x1995x1940</p>
                                        <p>VK 50 VD</p>
                                        <p>552</p>
                                        <p>400 @5800</p>
                                        <p>57.1@4000</p>
                                        <p>ECCS Electronic fuel injection control Hight Octane Gasoline</p>
                                        <p>7 speed AT</p>
                                        <p>SPEED Response TOPS Streening</p>
                                        <p>100+40L</p>
                                        <p>Power asisted,4-wheels,ABS,EBD,BA</p>
                                        <p>Ventilated Disc</p>
                                        <p>Ventilated Disc with</p>
                                        <p>HBMC (kinetie)</p>
                                        <p>275/60 R20;Machine cut alloy</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tabs3-2">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="title2">EXTRA FEATURES</div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <ul class="ul3">
                                                <li><a href="#">Alloy Wheels</a></li>
                                                <li><a href="#">Anti-Lock Brakes (ABS)</a></li>
                                                <li><a href="#">Anti-Theft</a></li>
                                                <li><a href="#">Anti-Starter</a></li>
                                                <li><a href="#">Security System</a></li>
                                                <li><a href="#">Air Conditioning</a></li>
                                                <li><a href="#">Alloy Wheels</a></li>
                                                <li><a href="#">Anti-Lock Brakes (ABS)</a></li>
                                                <li><a href="#">Anti-Theft</a></li>
                                                <li><a href="#">Anti-Starter</a></li>
                                                <li><a href="#">Security System</a></li>
                                                <li><a href="#">Air Conditioning</a></li>
                                                <li><a href="#">Alloy Wheels</a></li>
                                                <li><a href="#">Anti-Lock Brakes (ABS)</a></li>
                                                <li><a href="#">Anti-Theft</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul class="ul3">
                                                <li><a href="#">Alloy Wheels</a></li>
                                                <li><a href="#">Anti-Lock Brakes (ABS)</a></li>
                                                <li><a href="#">Anti-Theft</a></li>
                                                <li><a href="#">Anti-Starter</a></li>
                                                <li><a href="#">Security System</a></li>
                                                <li><a href="#">Air Conditioning</a></li>
                                                <li><a href="#">Alloy Wheels</a></li>
                                                <li><a href="#">Anti-Lock Brakes (ABS)</a></li>
                                                <li><a href="#">Anti-Theft</a></li>
                                                <li><a href="#">Anti-Starter</a></li>
                                                <li><a href="#">Security System</a></li>
                                                <li><a href="#">Air Conditioning</a></li>
                                                <li><a href="#">Alloy Wheels</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php include 'modules/carscomment.php'; ?>
                    </div>
                </div>
                <?php include 'modules/carsquotation.php'; ?>
            </div>
        </div>
        <?php include 'modules/carssimilar.php'; ?>
    </div>
    </div>
<?php include 'modules/suggestions.php'; ?>


<?php include 'modules/footer.php'; ?>