<?php include 'modules/header.php'; ?>


    <div class="top3-wrapper">
        <div class="container">
            <div class="top3 clearfix">
                <div class="tabs-wrapper">
                    <div class="txt">SELECT VIEW</div>
                    <div class="tabs1-wrapper">
                        <ul class="tabs clearfix" data-tabgroup="first-tab-group">
                            <li class="active"><a href="#tabs2-1"><i class="fa fa-list"></i></a></li>
                            <li><a href="#tabs2-2"><i class="fa fa-th"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="sort-wrapper">
                    <div class="txt">sort by</div>
                    <div class="select1_wrapper">
                        <label>Sort</label>
                        <div class="select1_inner">
                            <select class="select2 select" style="width: 100%">
                                <option value="1">Last Added</option>
                                <option value="2">Popular</option>
                                <option value="3">Price</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumbs1_wrapper">
        <div class="container">
            <div class="breadcrumbs1"><a href="index.php">Home</a><span></span>Search Results</div>
        </div>
    </div>
    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-3 col-md-push-9 column-sidebar">
                    <div class="banner" id="banner">
                        <div class="caption">
                            <div class="txt3"><a href="#" class="btn-default btn2a">REQUEST FOR<br> QUOATATION NOW</a>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar-form-wrapper" id="banner1">
                        <div class="sidebar-form">
                            <form action="javascript:;" class="form2">
                                <div class="select1_wrapper">
                                    <label>VEHICLE TYPE</label>
                                    <div class="select1_inner">
                                        <select class="select2 select" style="width: 100%">
                                            <option value="1">All vehicles</option>
                                            <option value="2">Passenger cars</option>
                                            <option value="3">Sport utility vehicle (SUV)</option>
                                            <option value="4">Trucks</option>
                                            <option value="5">Busses, Vans and MPV’s</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="select1_wrapper">
                                    <label>VEHICLE BRAND</label>
                                    <div class="select1_inner">
                                        <select class="select2 select" style="width: 100%">
                                            <option value="1">Any Brand</option>
                                            <option value="2">TOYOTA</option>
                                            <option value="3">NISSAN</option>
                                            <option value="4">MITSUBISHI</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="select1_wrapper">
                                    <label>VEHICLE MODEL</label>
                                    <div class="select1_inner">
                                        <select class="select2 select" style="width: 100%">
                                            <option value="1">Any Model</option>
                                            <option value="2">PATROL</option>
                                            <option value="3">PAJERO</option>
                                            <option value="4">PRADO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="select1_wrapper">
                                    <label>YEAR MODEL</label>
                                    <div class="select1_inner">
                                        <select class="select2 select" style="width: 100%">
                                            <option value="1">Any Year</option>
                                            <option value="2">2016</option>
                                            <option value="3">2017</option>
                                            <option value="4">2018</option>
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn-default btn2">SUBMIT FILTERS</button>
                                <div class="reset-filters"><a href="#">RESET ALL FILTERS</a></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-9 col-md-pull-3 column-content">
                    <div id="first-tab-group" class="tabgroup">
                        <div id="tabs2-1">
                            <div class="car-view3-wrapper">
                                <div class="car-view1 clearfix">
                                    <figure><img src="images/b1.png" style="width: 322px;" alt=""
                                                 class="img-responsive"></figure>
                                    <div class="caption">
                                        <div class="top-info clearfix">
                                            <div class="info1">
                                                <div class="txt1">NISSAN PATROL MY 2018</div>
                                            </div>
                                            <div class="info2">
                                                <div class="txt3">AED 99,415</div>
                                            </div>
                                        </div>
                                        <div class="txt4">
                                            <!-- In a pickup market gone fancy, the Silverado sticks to its basic-truck
                                                recipe. The nuico steering is accurate, and the Silverado handles more like
                                                a big car than a big truck, la and the Silverado handles more like a big car
                                                than a big truck and the Silverado duoe handles more like a big car than a
                                                big truck. -->
                                            PATROL LE VK56 VS 7AT PTNMP18
                                        </div>
                                        <div class="bot-info clearfix">
                                            <!-- <div class="info3">
                                                <div class="txt5">250 HP</div>
                                                <div class="txt6">2018&nbsp; /&nbsp; 8-Speed Automatic&nbsp; /&nbsp;
                                                    Petrol
                                                </div>
                                                </div> -->
                                            <div class="info4">
                                                <div class="txt7">
                                                    <a href="car1.php" class="btn-default btn3">VIEW DETAILS</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="car-view3-wrapper">
                                <div class="car-view1 clearfix">
                                    <figure><img src="images/a1.png" style="width: 322px;" alt=""
                                                 class="img-responsive"></figure>
                                    <div class="caption">
                                        <div class="top-info clearfix">
                                            <div class="info1">
                                                <div class="txt1">NISSAN PATROL</div>
                                            </div>
                                            <div class="info2">
                                                <div class="txt3">AED 209,415</div>
                                            </div>
                                        </div>
                                        <div class="txt4">
                                            Patrol HT 4WD Safari TB48 AT lPBF 18
                                        </div>
                                        <div class="bot-info clearfix">
                                            <!-- <div class="info3">
                                                <div class="txt5">250 HP</div>
                                                <div class="txt6">2018&nbsp; /&nbsp; 8-Speed Automatic&nbsp; /&nbsp;
                                                    Petrol
                                                </div>
                                                </div> -->
                                            <div class="info4">
                                                <div class="txt7">
                                                    <a href="car2.php" class="btn-default btn3">VIEW DETAILS</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="car-view3-wrapper">
                                <div class="car-view1 clearfix">
                                    <figure><img src="images/c1.png" style="width: 322px;" alt=""
                                                 class="img-responsive"></figure>
                                    <div class="caption">
                                        <div class="top-info clearfix">
                                            <div class="info1">
                                                <div class="txt1">NISSAN PATROL</div>
                                            </div>
                                            <div class="info2">
                                                <div class="txt3">AED 99,415</div>
                                            </div>
                                        </div>
                                        <div class="txt4">
                                            PATROL SAFARI 1'B48 FF7 MT Pl 7 Special version
                                        </div>
                                        <div class="bot-info clearfix">
                                            <!-- <div class="info3">
                                                <div class="txt5">250 HP</div>
                                                <div class="txt6">2017&nbsp; /&nbsp; 8-Speed Automatic&nbsp; /&nbsp;
                                                    Petrol
                                                </div>
                                                </div> -->
                                            <div class="info4">
                                                <div class="txt7">
                                                    <a href="car3.php" class="btn-default btn3">VIEW DETAILS</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="car-view3-wrapper">
                                <div class="car-view1 clearfix">
                                    <figure><img src="images/d1.png" style="width: 322px;" alt=""
                                                 class="img-responsive"></figure>
                                    <div class="caption">
                                        <div class="top-info clearfix">
                                            <div class="info1">
                                                <div class="txt1">NISSAN PATROL</div>
                                            </div>
                                            <div class="info2">
                                                <div class="txt3">AED 99,415</div>
                                            </div>
                                        </div>
                                        <div class="txt4">
                                            Patrol HT 4WD Safari TB48 MT lPBF 18
                                        </div>
                                        <div class="bot-info clearfix">
                                            <!-- <div class="info3">
                                                <div class="txt5">250 HP</div>
                                                <div class="txt6">2017&nbsp; /&nbsp; 8-Speed Automatic&nbsp; /&nbsp;
                                                    Petrol
                                                </div>
                                                </div> -->
                                            <div class="info4">
                                                <div class="txt7">
                                                    <a href="car4.php" class="btn-default btn3">VIEW DETAILS</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pager_wrapper">
                                <ul class="pager clearfix">
                                    <li class="prev"><a href="#"></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li class="li"><a href="#">2</a></li>
                                    <li class="li"><a href="#">3</a></li>
                                    <li class="li"><a href="#">4</a></li>
                                    <li class="dots">....</li>
                                    <li class="next"><a href="#"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div id="tabs2-2">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="car-view4-wrapper">
                                        <div class="car-view2 clearfix">
                                            <figure><img src="images/b1.png" alt="" class="img-responsive"></figure>
                                            <div class="caption">
                                                <div class="top-info clearfix">
                                                    <div class="info1">
                                                        <div class="txt1">NISSAN PATROL MY 2018</div>
                                                    </div>
                                                </div>
                                                <div class="txt3">
                                                    <!-- In a pickup market gone fancy, the Silverado sticks to
                                                        its basic-truck recipe. The steering is accurate ... -->
                                                    PATROL LE VK56 VS 7AT PTNMP18 AT lPBF 18
                                                </div>
                                                <div class="bot-info clearfix">
                                                    <div class="info2">
                                                        <div class="txt6">AED 99,415</div>
                                                    </div>
                                                    <div class="info3">
                                                        <div class="txt7">
                                                            <a href="car1.php" class="btn-default btn3">VIEW DETAILS</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="car-view4-wrapper">
                                        <div class="car-view2 clearfix">
                                            <figure><img src="images/a1.png" alt="" class="img-responsive"></figure>
                                            <div class="caption">
                                                <div class="top-info clearfix">
                                                    <div class="info1">
                                                        <div class="txt1">NISSAN PATROL</div>
                                                    </div>
                                                </div>
                                                <div class="txt3">
                                                    <!-- In a pickup market gone fancy, the Silverado sticks to
                                                        its basic-truck recipe. The steering is accurate ... -->
                                                    Patrol HT 4WD Safari TB48 AT lPBF 18
                                                </div>
                                                <div class="bot-info clearfix">
                                                    <div class="info2">
                                                        <div class="txt6">AED 209,415</div>
                                                    </div>
                                                    <div class="info3">
                                                        <div class="txt7">
                                                            <a href="car2.php" class="btn-default btn3">VIEW DETAILS</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="car-view4-wrapper">
                                        <div class="car-view2 clearfix">
                                            <figure><img src="images/c1.png" alt="" class="img-responsive"></figure>
                                            <div class="caption">
                                                <div class="top-info clearfix">
                                                    <div class="info1">
                                                        <div class="txt1">NISSAN PATROL</div>
                                                    </div>
                                                </div>
                                                <div class="txt3">
                                                    <!-- In a pickup market gone fancy, the Silverado sticks to
                                                        its basic-truck recipe. The steering is accurate ... -->
                                                    PATROL SAFARI 1'B48 FF7 MT Pl 7 Special version
                                                </div>
                                                <div class="bot-info clearfix">
                                                    <div class="info2">
                                                        <div class="txt6">AED 99,415</div>
                                                    </div>
                                                    <div class="info3">
                                                        <div class="txt7">
                                                            <a href="car3.php" class="btn-default btn3">VIEW DETAILS</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="car-view4-wrapper">
                                        <div class="car-view2 clearfix">
                                            <figure><img src="images/d1.png" alt="" class="img-responsive"></figure>
                                            <div class="caption">
                                                <div class="top-info clearfix">
                                                    <div class="info1">
                                                        <div class="txt1">NISSAN PATROL</div>
                                                    </div>
                                                </div>
                                                <div class="txt3">
                                                    <!-- In a pickup market gone fancy, the Silverado sticks to
                                                        its basic-truck recipe. The steering is accurate ... -->
                                                    PATROL SAFARI 1'B48 FF7 MT Pl 7 Special version
                                                </div>
                                                <div class="bot-info clearfix">
                                                    <div class="info2">
                                                        <div class="txt6">AED 99,415</div>
                                                    </div>
                                                    <div class="info3">
                                                        <div class="txt7">
                                                            <a href="car4.php" class="btn-default btn3">VIEW DETAILS</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="car-view4-wrapper">
                                        <div class="car-view2 clearfix">
                                            <figure><img src="images/d1.png" alt="" class="img-responsive"></figure>
                                            <div class="caption">
                                                <div class="top-info clearfix">
                                                    <div class="info1">
                                                        <div class="txt1">NISSAN PATROL</div>
                                                    </div>
                                                </div>
                                                <div class="txt3">
                                                    <!-- In a pickup market gone fancy, the Silverado sticks to
                                                        its basic-truck recipe. The steering is accurate ... -->
                                                    PATROL SAFARI 1'B48 FF7 MT Pl 7 Special version
                                                </div>
                                                <div class="bot-info clearfix">
                                                    <div class="info2">
                                                        <div class="txt6">AED 99,415</div>
                                                    </div>
                                                    <div class="info3">
                                                        <div class="txt7">
                                                            <a href="car4.php" class="btn-default btn3">VIEW DETAILS</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="car-view4-wrapper">
                                        <div class="car-view2 clearfix">
                                            <figure><img src="images/d1.png" alt="" class="img-responsive"></figure>
                                            <div class="caption">
                                                <div class="top-info clearfix">
                                                    <div class="info1">
                                                        <div class="txt1">NISSAN PATROL</div>
                                                    </div>
                                                </div>
                                                <div class="txt3">
                                                    <!-- In a pickup market gone fancy, the Silverado sticks to
                                                        its basic-truck recipe. The steering is accurate ... -->
                                                    PATROL SAFARI 1'B48 FF7 MT Pl 7 Special version
                                                </div>
                                                <div class="bot-info clearfix">
                                                    <div class="info2">
                                                        <div class="txt6">AED 99,415</div>
                                                    </div>
                                                    <div class="info3">
                                                        <div class="txt7">
                                                            <a href="car4.php" class="btn-default btn3">VIEW DETAILS</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pager_wrapper">
                                <ul class="pager clearfix">
                                    <li class="prev"><a href="#"></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li class="li"><a href="#">2</a></li>
                                    <li class="li"><a href="#">3</a></li>
                                    <li class="li"><a href="#">4</a></li>
                                    <li class="dots">....</li>
                                    <li class="next"><a href="#"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include 'modules/suggestions.php'; ?>

    <script type="text/javascript">
        window.onscroll = function () {
            scrollFunction()
        };


        function scrollFunction() {

            if (document.body.scrollTop > 210 || document.documentElement.scrollTop > 210) {
                document.getElementById("banner").style.position = "fixed";
                document.getElementById("banner").style.width = "270px";
                document.getElementById("banner").style.zIndex = "45";
                document.getElementById("banner").style.top = "80px";
                document.getElementById("banner1").style.marginTop = "170px";


            } else {
                document.getElementById("banner").style.position = "relative";
                document.getElementById("banner").style.width = "270px";
                document.getElementById("banner").style.zIndex = "45";
                document.getElementById("banner").style.top = "0px";
                document.getElementById("banner1").style.marginTop = "0px";


            }
        }


    </script>

    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>


<?php include 'modules/footer.php'; ?>