<!DOCTYPE html>
<html lang="en">
<head>
    <title>Alwan Cars</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Your description">
    <meta name="keywords" content="Your keywords">
    <meta name="author" content="Your name">

    <link href="css/all.css" rel="stylesheet">

    <script src="js/bootstrap.min"></script>
    <script src="js/cform"></script>
    <script src="js/googlemap"></script>
    <script src="js/jquery.equalheights"></script>
    <script src="js/jquery.mobile.customized.min"></script>
    <script src="js/jquery.parallax-1.1.3"></script>
    <script src="js/main"></script>
    <script src="js/select2"></script>
    <script src="js/superfish"></script>
    <script src="js/jquery.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery-migrate-1.2.1.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/superfish.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="js/select2.js"></script>
    <script src="js/jquery.superslides.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/jquery.ui.totop.js"></script>
    <script src="js/jquery.caroufredsel.js"></script>
    <script src="js/jquery.touchSwipe.min.js"></script>
    <script src="js/jquery.parallax-1.1.3.resize.js"></script>
    <script src="js/SmoothScroll.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false.js"></script>
    <script src="js/googlemap1.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.mobile-1.4.5.js"></script>


    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="not-front page-listing">

<style>
    .mob-hamb {
        overflow: auto;
        background-color: white;
        position: fixed;
        right: -75%;
        top: 0px;
        height: 100%;
        width: 75%;
        z-index: 2000;
        transition: all 700ms;
    }
    .mob-hamb a{
        text-decoration: none;
    }
    .mob-hamb-active {
        transition: all 700ms;
        right: 0% !important;
    }

    .mob-hamb-x {
        font-size: 47px;
        padding: 0px 20px;
        color: #a8cf45;
    }

    .mob-hamb-fr, .mob-hamb-fr-, .mob-hamb-se, .mob-hamb-se-, .mob-hamb-th div {
        padding: 10px;
        width: 100%;
        background-color: #a8cf45;
        color: white;
        font-size: 20px;
        margin: 2px 0px;
        transition: all 700ms;
        -webkit-box-shadow: 0px 0px 15px -4px rgba(0, 0, 0, 0.75);
        -moz-box-shadow: 0px 0px 15px -4px rgba(0, 0, 0, 0.75);
        box-shadow: 0px 0px 15px -4px rgba(0, 0, 0, 0.75);
    }

    .mob-hamb-se {
        display: none;
    }

    .mob-hamb-th {

        display: none;
    }

    .mob-hamb a {
        color: white;
        position: relative;
        width: 100%;
        height: 100%;

    }

    .view-active {
        display: block;
    }

    .mob-hamb-add-margin {
        margin-left: 30px !important;
        transition: all 700ms;
    }

    .mob-hamb-add-margin-x2 {
        margin-left: 60px !important;
        transition: all 700ms;
    }

    .mob-hamb-remove-margin {
        margin-left: -30px !important;
        transition: all 700ms;
    }
    
    @media(max-width: 767px){
        .temp{
            display: none;
        }
    }


</style>


<div class="mob-hamb">
    <div class="mob-hamb-x"><i class="fa fa-close"></i></div>
    <a href="index.php">
        <div class="mob-hamb-fr">Home</div>
    </a>
    <div class="mob-hamb-fr">
        <div class="mob-hamb-fr_">
            Our cars<i class="fa fa-caret-down"></i>
        </div>
        <div class="mob-hamb-se">
            <div class="mob-hamb-se_">
                Local sales and GCC<i class="fa fa-caret-down"></i>
            </div>
            <div class="mob-hamb-th">
                <a href="cars.php">
                    <div>All vehicles</div>
                </a>
                <a href="cars.php">
                    <div>Passenger cars</div>
                </a>
                <a href="cars.php">
                    <div>Trucks</div>
                </a>
                <a href="cars.php">
                    <div>Pick-ups</div>
                </a>
                <a href="cars.php">
                    <div>Busses, Vans and MPV’s</div>
                </a>
            </div>
        </div>
        <div class="mob-hamb-se">
            <div class="mob-hamb-se_">
                Export sales <i class="fa fa-caret-down"></i>
            </div>
            <div class="mob-hamb-th">
                <a href="cars.php">
                    <div>All vehicles</div>
                </a>
                <a href="cars.php">
                    <div>Passenger cars</div>
                </a>
                <a href="cars.php">
                    <div>Trucks</div>
                </a>
                <a href="cars.php">
                    <div>Pick-ups</div>
                </a>
                <a href="cars.php">
                    <div>Busses, Vans and MPV’s</div>
                </a>
            </div>
        </div>
    </div>
    <div class="mob-hamb-fr">
        <div class="mob-hamb-fr_">
            Our cars<i class="fa fa-caret-down"></i>
        </div>
        <div class="mob-hamb-se">
            <div class="mob-hamb-se_">
                Local sales and GCC<i class="fa fa-caret-down"></i>
            </div>
            <div class="mob-hamb-th">
                <a href="cars.php">
                    <div>All vehicles</div>
                </a>
                <a href="cars.php">
                    <div>Passenger cars</div>
                </a>
                <a href="cars.php">
                    <div>Trucks</div>
                </a>
                <a href="cars.php">
                    <div>Pick-ups</div>
                </a>
                <a href="cars.php">
                    <div>Busses, Vans and MPV’s</div>
                </a>
            </div>
        </div>
        <div class="mob-hamb-se">
            <div class="mob-hamb-se_">
                <a>Export sales <i class="fa fa-caret-down"></i></a>
            </div>
            <div class="mob-hamb-th">
                <a href="cars.php">
                    <div>All vehicles</div>
                </a>
                <a href="cars.php">
                    <div>Passenger cars</div>
                </a>
                <a href="cars.php">
                    <div>Trucks</div>
                </a>
                <a href="cars.php">
                    <div>Pick-ups</div>
                </a>
                <a href="cars.php">
                    <div>Busses, Vans and MPV’s</div>
                </a>
            </div>
        </div>
    </div>
    <a href="about.php">
        <div class="mob-hamb-fr">About us</div>
    </a>
    <a href="news.php">
        <div class="mob-hamb-fr">News</div>
    </a>
    <a href="#">
        <div class="mob-hamb-fr">Career</div>
    </a>
    <a href="contact.php">
        <div class="mob-hamb-fr">Contact us</div>
    </a>
</div>


<div id="main">
    <div class="top0">
        <div class="container">
            <div class="block-left">
                <div class="address1"><span aria-hidden="true" class="ei icon_mail"></span>info@alwancars-uae.com</div>
                <div class="phone1"><span aria-hidden="true" class="ei icon_phone"></span>00971 (4)320 9922</div>
                <div class="social_wrapper">
                    <ul class="social clearfix">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="block-right">
                <div class="lang1">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">EN<span
                                    class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a class="ar" href="#">AR</a></li>
                            <li><a class="ru" href="#">RU</a></li>
                            <li><a class="pr" href="#">PR</a></li>
                            <li><a class="fr" href="#">FR</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="top1">
        <div class="top2_wrapper" id="top2">
            <div class="container">
                <div class="top2 clearfix">
                    <header>
                        <div class="logo_wrapper">
                            <a href="../alwan" class="logo scroll-to">
                                <img src="images/logo.png" alt="" class="img-responsive">
                            </a>
                        </div>
                    </header>
                    <div class="navbar navbar_ navbar-default">
                        <div class="mob-hamb-button">
                            <button type="button" class="navbar-toggle "
                                    data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="navbar-collapse navbar-collapse_ collapse">
                            <ul class="nav navbar-nav sf-menu clearfix">
                                <li class="sub-menu-out"><a href="index.php">Home</a>
                                    <div class="nav-hov-bckg-out"></div>
                                </li>
                                <li class="sub-menu sub-menu-1">
                                    <a>Our cars<i class="fa fa-caret-down"></i></a>
                                    <ul>
                                        <li class="sub-menu sub-menu-in sub-menu-1">
                                            <a>Local sales and GCC<i class="fa fa-caret-down"></i></a>
                                            <div class="nav-hov-bckg"></div>
                                            <ul class="inner-menu-3">
                                                <li class="sub-menu-in-in"><a href="cars.php">All vehicles</a>
                                                    <div class="nav-hov-bckg-in"></div>
                                                </li>
                                                <li class="sub-menu-in-in"><a href="cars.php">Passenger cars</a>
                                                    <div class="nav-hov-bckg-in"></div>
                                                </li>
                                                <li class="sub-menu-in-in"><a href="cars.php">Trucks</a>
                                                    <div class="nav-hov-bckg-in"></div>
                                                </li>
                                                <li class="sub-menu-in-in"><a href="cars.php">Pick-ups</a>
                                                    <div class="nav-hov-bckg-in"></div>
                                                </li>
                                                <li class="sub-menu-in-in"><a href="cars.php">Busses, Vans and MPV’s</a>
                                                    <div class="nav-hov-bckg-in"></div>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="sub-menu sub-menu-in sub-menu-1">
                                            <a>Export sales <i class="fa fa-caret-down"></i></a>
                                            <div class="nav-hov-bckg"></div>
                                            <ul class="inner-menu-3">
                                                <li class="sub-menu-in-in"><a href="cars.php">All vehicles</a>
                                                    <div class="nav-hov-bckg-in"></div>
                                                </li>
                                                <li class="sub-menu-in-in"><a href="cars.php">Passenger cars</a>
                                                    <div class="nav-hov-bckg-in"></div>
                                                </li>
                                                <li class="sub-menu-in-in"><a href="cars.php">Trucks</a>
                                                    <div class="nav-hov-bckg-in"></div>
                                                </li>
                                                <li class="sub-menu-in-in"><a href="cars.php">Pick-ups</a>
                                                    <div class="nav-hov-bckg-in"></div>
                                                </li>
                                                <li class="sub-menu-in-in"><a href="cars.php">Busses, Vans and MPV’s</a>
                                                    <div class="nav-hov-bckg-in"></div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li class="sub-menu-out"><a href="about.php">About us</a>
                                    <div class="nav-hov-bckg-out"></div></li>
                                <li class="sub-menu-out"><a href="news.php">News</a>
                                    <div class="nav-hov-bckg-out"></div></li>
                                <li class="sub-menu-out"><a href="#">Career</a>
                                    <div class="nav-hov-bckg-out"></div></li>
                                <li class="sub-menu-out"><a href="contact.php">Contact us</a>
                                    <div class="nav-hov-bckg-out"></div></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        var speed = 10;
        $('.mob-hamb-button').click(function () {
            $('.mob-hamb').addClass("mob-hamb-active");
        });
        $('.mob-hamb-x').click(function () {
            $('.mob-hamb').removeClass("mob-hamb-active");
            $('div').removeClass("mob-hamb-remove-margin");
            $('div').removeClass("mob-hamb-add-margin");
            $('div').removeClass("mob-hamb-add-margin-x2");
            $('.mob-hamb-se').slideUp(speed);
            $(".mob-hamb-th").slideUp(speed);
        });

        $(".mob-hamb").on("swipe", swipeHandler);

        // Callback function references the event target and adds the 'swipe' class to it
        function swipeHandler(event) {
            $('.mob-hamb').removeClass("mob-hamb-active");
            $('div').removeClass("mob-hamb-remove-margin");
            $('div').removeClass("mob-hamb-add-margin");
            $('div').removeClass("mob-hamb-add-margin-x2");
            $('.mob-hamb-se').slideUp(speed);
            $(".mob-hamb-th").slideUp(speed);
        }

        $('.mob-hamb-fr_').click(function () {


            if ($(this).parent(".mob-hamb-fr").siblings(".mob-hamb-fr").children(".mob-hamb-se").hasClass("mob-hamb-remove-margin")) {
                $(this).parent(".mob-hamb-fr").siblings(".mob-hamb-fr").children('.mob-hamb-se').slideToggle(300);
                $(this).parent(".mob-hamb-fr").siblings(".mob-hamb-fr").children('.mob-hamb-se').toggleClass("mob-hamb-remove-margin");

                $(this).siblings('.mob-hamb-se').slideToggle(speed);
                $(this).siblings('.mob-hamb-se').toggleClass("mob-hamb-remove-margin");
                $('div').removeClass("mob-hamb-add-margin-x2");
                $(".mob-hamb-th").removeClass("mob-hamb-remove-margin");
                $('.mob-hamb-th').slideUp(speed);

            }
            else {
                $(this).siblings('.mob-hamb-se').slideToggle(speed);
                $(".mob-hamb-fr").toggleClass("mob-hamb-add-margin");
                $(this).siblings('.mob-hamb-se').toggleClass("mob-hamb-remove-margin");

                $(".mob-hamb-th").removeClass("mob-hamb-remove-margin");
                $(".mob-hamb-th").slideUp(speed);
                $(".mob-hamb-fr").removeClass("mob-hamb-add-margin-x2");
            }


        })

        $('.mob-hamb-se_').click(function () {
            if ($(this).parent(".mob-hamb-se").siblings(".mob-hamb-se").children(".mob-hamb-th").hasClass("mob-hamb-remove-margin")) {
                $(this).parent(".mob-hamb-se").siblings(".mob-hamb-se").children('.mob-hamb-th').slideToggle(speed);
                $(this).parent(".mob-hamb-se").siblings(".mob-hamb-se").children('.mob-hamb-th').toggleClass("mob-hamb-remove-margin");
                $(this).siblings('.mob-hamb-th').slideToggle(speed);
                $(this).siblings('.mob-hamb-th').toggleClass("mob-hamb-remove-margin");
            }
            else {
                $(this).siblings('.mob-hamb-th').slideToggle(speed);
                $(".mob-hamb-se").toggleClass("mob-hamb-add-margin-x2");
                $(".mob-hamb-fr").toggleClass("mob-hamb-add-margin-x2");
                $(this).siblings(".mob-hamb-th").toggleClass("mob-hamb-remove-margin");
            }


        })
    </script>