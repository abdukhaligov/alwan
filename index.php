<?php include 'modules/header.php'; ?>


    <div id="welcome">
        <div id="parallax1" class="parallax">
            <div class="bg1 parallax-bg"></div>
            <div class="parallax-content">
                <div class="container">
                    <div class="wl_bg row">
                        <div class="col-sm-6">
                            <div class="txt2">
                                Established in 1990, Alwan Cars LLC has become one of the leading Automotive trading
                                companies in the UAE, which specializes in Brand New Vehicles Re-Exportation. With
                                humble beginnings as a small used cars trading company in Sharjah, it has emerged today
                                as a stable, competitive and trusted company in the Automotive industry reaching the
                                markets as far as Asia, Europe, Middle East, and Africa.
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="txt3">
                                <span>1650</span> CARS</br>
                                <span>1650</span> BRANDS</br>
                                <span>25</span> MODELS</br>
                            </div>
                        </div>
                    </div>
                    <div class="wl_bt center">
                        <a href="showroom/index.php" class="btn-default btn-form1-submit1">
                            <span>VISIT SHOWROOM</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="banner_index">
        <div class="caption">
            <div class="txt3"><a href="#" class="btn-default btn2 btn-220">REQUEST FOR QUOATATION NOW</a></div>
        </div>
    </div>
    <div class="banner_index_fixed_container">
    </div>
    <div id="searchbar">
        <div class="container">
            <div class="booking-wrapper">
                <div class="booking">
                    <form action="javascript:;" class="form1">
                        <div class="row">
                            <div class="col1">
                                <div class="select1_wrapper">
                                    <label>VEHICLE TYPE</label>
                                    <div class="select1_inner">
                                        <select class="select2 select" style="width: 100%">
                                            <option value="1">All vehicles</option>
                                            <option value="2">Passenger cars</option>
                                            <option value="3">Sport utility vehicle (SUV)</option>
                                            <option value="4">Trucks</option>
                                            <option value="5">Busses, Vans and MPV’s</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col1">
                                <div class="select1_wrapper">
                                    <label>VEHICLE BRAND</label>
                                    <div class="select1_inner">
                                        <select class="select2 select" style="width: 100%">
                                            <option value="1">Any Brand</option>
                                            <option value="2">TOYOTA</option>
                                            <option value="3">NISSAN</option>
                                            <option value="4">MITSUBISHI</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col1">
                                <div class="select1_wrapper">
                                    <label>VEHICLE MODEL</label>
                                    <div class="select1_inner">
                                        <select class="select2 select" style="width: 100%">
                                            <option value="1">Any Model</option>
                                            <option value="2">PATROL</option>
                                            <option value="3">PAJERO</option>
                                            <option value="4">PRADO</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col1">
                                <div class="select1_wrapper">
                                    <label>YEAR MODEL</label>
                                    <div class="select1_inner">
                                        <select class="select2 select" style="width: 100%">
                                            <option value="1">Any Year</option>
                                            <option value="2">2016</option>
                                            <option value="3">2017</option>
                                            <option value="4">2018</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col2">
                                <div id="slider-range-wrapper">
                                    <!--
                                        <div class="txt">PRICE RANGE</div>

                                        <div id="slider-range"></div>

                                        <div class="clearfix">
                                            <input type="text" id="amount" readonly>
                                            <input type="text" id="amount2" readonly>
                                        </div>
                                        -->
                                </div>
                            </div>
                            <div class="col3">
                                <div class="adv-serach"><a class="" href="#">ADVANCED SEARCH</a></div>
                                <button type="submit" class="btn-default btn-form1-submit2">
                                    <span>SEARCH FOR A VEHICLE</span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="home" class="">
        <div id="slides_wrapper" class="">
            <div id="slides">
                <ul class="slides-container">
                    <li class="nav1">
                        <img src="images/head1.jpg" alt="" class="img">
                        <div class="caption">
                            <div class="container">
                                <!--<div class="txt1"><span>FIND YOUR DREAM CAR</span></div>-->
                                <div class="txt2">MITSUBISHI PAJERO PLUS</div>
                                <div class="txt3"><span>AED 90,000</span> MODEL 2017</div>
                                <div class="link1"><a href="#" class="slider-link1"><span>SEE DETAILS</span></a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav2">
                        <img src="images/head2.jpg" alt="" class="img">
                        <div class="caption">
                            <div class="container">
                                <!--<div class="txt1"><span>FIND YOUR DREAM CAR</span></div>-->
                                <div class="txt2">MITSUBISHI PAJERO PLUS</div>
                                <div class="txt3"><span>AED 90,000</span> MODEL 2017</div>
                                <div class="link1">
                                    <a href="#" class="slider-link1"><span>SEE DETAILS</span></a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav3">
                        <img src="images/head3.jpg" alt="" class="img">
                        <div class="caption">
                            <div class="container">
                                <!--<div class="txt1"><span>FIND YOUR DREAM CAR</span></div>-->
                                <div class="txt2">MITSUBISHI PAJERO PLUS</div>
                                <div class="txt3"><span>AED 90,000</span> MODEL 2017</div>
                                <div class="link1">
                                    <a href="#" class="slider-link1"><span>SEE DETAILS</span></a>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <nav class="slides-navigation">
                    <a href="#" class="next"></a>
                    <a href="#" class="prev"></a>
                </nav>
            </div>
        </div>
    </div>
    <div id="info">
        <div class="info-wrapper">
            <div class="container">
                <div class="info-inner">
                    <div class="info1">
                        <div class="info1-inner animated" data-animation="fadeInDown" data-animation-delay="200">
                            <img src="images/ic1.png" alt="" class="img1">
                            <div class="caption">
                                <div class="txt1"><span class="animated-number" data-duration="2000"
                                                        data-animation-delay="0">1250</span></div>
                                <div class="txt2">OUR STOCK</div>
                            </div>
                        </div>
                    </div>
                    <div class="info1">
                        <div class="info1-inner animated" data-animation="fadeInDown" data-animation-delay="250">
                            <img src="images/ic2.png" alt="" class="img1">
                            <div class="caption">
                                <div class="txt1"><span class="animated-number" data-duration="2000"
                                                        data-animation-delay="0">21020</span>
                                </div>
                                <div class="txt2">CARS HAVE BEEN SOLD</div>
                            </div>
                        </div>
                    </div>
                    <div class="info1">
                        <div class="info1-inner animated" data-animation="fadeInDown" data-animation-delay="300">
                            <img src="images/ic3.png" alt="" class="img1">
                            <div class="caption">
                                <div class="txt1"><span class="animated-number" data-duration="2000"
                                                        data-animation-delay="0">9753</span></div>
                                <div class="txt2">WEBSITE VISITORSF</div>
                            </div>
                        </div>
                    </div>
                    <div class="info1">
                        <div class="info1-inner animated" data-animation="fadeInDown" data-animation-delay="350">
                            <img src="images/ic4.png" alt="" class="img1">
                            <div class="caption">
                                <div class="txt1"><span class="animated-number" data-duration="2000"
                                                        data-animation-delay="0">8</span></div>
                                <div class="txt2">STORES</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="intro">
        <div class="container">
            <div class="title1"><span>COMING SOON</span></div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="thumb1">
                        <div class="thumbnail clearfix">
                            <figure>
                                <a href="#">
                                    <img src="images/e1.png" alt="" class="img-responsive">
                                </a>
                            </figure>
                            <div class="caption">
                                <div class="txt2">2017 SOME SUV CAR</div>
                                <div class="txt3">Curabitur libero. Donec facilisis velit eu est. Phasellus cons quat.
                                    Aenean vitae quam. Vivamus et nunc. Nunc consequ
                                    sem velde metus imperdiet lacinia.
                                </div>
                                <div class="link"><a href="#" class="btn-default btn1"><span>READ MORE</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="thumb1">
                        <div class="thumbnail clearfix">
                            <figure>
                                <a href="#">
                                    <img src="images/b1.png" alt="" class="img-responsive">
                                </a>
                            </figure>
                            <div class="caption">
                                <div class="txt2">2017 SOME SUV CAR</div>
                                <div class="txt3">Curabitur libero. Donec facilisis velit eu est. Phasellus cons quat.
                                    Aenean vitae quam. Vivamus et nunc. Nunc consequ
                                    sem velde metus imperdiet lacinia.
                                </div>
                                <div class="link"><a href="#" class="btn-default btn1"><span>READ MORE</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="thumb1">
                        <div class="thumbnail clearfix">
                            <figure>
                                <a href="#">
                                    <img src="images/a1.png" alt="" class="img-responsive">
                                </a>
                            </figure>
                            <div class="caption">
                                <div class="txt2">2017 SOME SUV CAR</div>
                                <div class="txt3">Curabitur libero. Donec facilisis velit eu est. Phasellus cons quat.
                                    Aenean vitae quam. Vivamus et nunc. Nunc consequ
                                    sem velde metus imperdiet lacinia.
                                </div>
                                <div class="link"><a href="#" class="btn-default btn1"><span>READ MORE</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="best">
        <div class="container">
            <div class="title1"><span>BEST OFFERS FROM ALWAN CARS</span></div>
            <div class="tabs1">
                <div class="tabs1_tabs">
                    <ul>
                        <li class="active"><a href="#tabs1-1">VEHICLE IN HIGH DEMAND</a></li>
                        <li><a href="#tabs1-2">NEW ARRIVAL</a></li>
                    </ul>
                </div>
                <div class="tabs1_content">
                    <div id="tabs1-1">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="thumb2">
                                            <div class="thumbnail clearfix">
                                                <figure>
                                                    <a href="#">
                                                        <img src="images/b1.png" alt=""
                                                             class="img-responsive">
                                                    </a>
                                                </figure>
                                                <div class="caption">
                                                    <div class="txt2">Nissan Patrol</div>
                                                    <div class="info clearfix">
                                                        <span class="price">AED 24,380</span>
                                                        <span class="speed">250 HP</span>
                                                    </div>
                                                    <div class="txt3">
                                                        2015 • Automatic • White • Petrol
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="thumb2">
                                            <div class="thumbnail clearfix">
                                                <figure>
                                                    <a href="#">
                                                        <img src="images/a1.png" alt=""
                                                             class="img-responsive">
                                                    </a>
                                                </figure>
                                                <div class="caption">
                                                    <div class="txt2">Mitsubishi Pajero</div>
                                                    <div class="info clearfix">
                                                        <span class="price">AED 95,900</span>
                                                        <span class="speed">250 HP</span>
                                                    </div>
                                                    <div class="txt3">
                                                        2016 • Manual • Red • Petrol
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="thumb2">
                                            <div class="thumbnail clearfix">
                                                <figure>
                                                    <a href="#">
                                                        <img src="images/e1.png" alt=""
                                                             class="img-responsive">
                                                    </a>
                                                </figure>
                                                <div class="caption">
                                                    <div class="txt2">Nissan Patrol</div>
                                                    <div class="info clearfix">
                                                        <span class="price">AED 98,995</span>
                                                        <span class="speed">250 HP</span>
                                                    </div>
                                                    <div class="txt3">
                                                        2015 • Automatic • Blue • Petrol
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="thumb2">
                                            <div class="thumbnail clearfix">
                                                <figure>
                                                    <a href="#">
                                                        <img src="images/a1.png" alt=""
                                                             class="img-responsive">
                                                    </a>
                                                </figure>
                                                <div class="caption">
                                                    <div class="txt2">Mitsubishi Pajero</div>
                                                    <div class="info clearfix">
                                                        <span class="price">AED 31,900</span>
                                                        <span class="speed">250 HP</span>
                                                    </div>
                                                    <div class="txt3">
                                                        2017 • Automatic • Dark Red • Petrol
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="thumb2">
                                            <div class="thumbnail clearfix">
                                                <figure>
                                                    <a href="#">
                                                        <img src="images/d1.png" alt=""
                                                             class="img-responsive">
                                                    </a>
                                                </figure>
                                                <div class="caption">
                                                    <div class="txt2">NISSAN PATROL</div>
                                                    <div class="info clearfix">
                                                        <span class="price">AED 18,995</span>
                                                        <span class="speed">250 HP</span>
                                                    </div>
                                                    <div class="txt3">
                                                        2016 • Automatic • Yellow • Diesel
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="thumb2">
                                            <div class="thumbnail clearfix">
                                                <figure>
                                                    <a href="#">
                                                        <img src="images/a1.png" alt=""
                                                             class="img-responsive">
                                                    </a>
                                                </figure>
                                                <div class="caption">
                                                    <div class="txt2">TOYOTA</div>
                                                    <div class="info clearfix">
                                                        <span class="price">AED 64,380</span>
                                                        <span class="speed">249 HP</span>
                                                    </div>
                                                    <div class="txt3">
                                                        2017 • Automatic • Green • Diesel
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tabs1-2">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="thumb2">
                                            <div class="thumbnail clearfix">
                                                <figure>
                                                    <a href="#">
                                                        <img src="images/b1.png" alt=""
                                                             class="img-responsive">
                                                    </a>
                                                </figure>
                                                <div class="caption">
                                                    <div class="txt2">Mitsubishi Pajero</div>
                                                    <div class="info clearfix">
                                                        <span class="price">AED 31,900</span>
                                                        <span class="speed">250 HP</span>
                                                    </div>
                                                    <div class="txt3">
                                                        2017 • Automatic • Dark Red • Petrol
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="thumb2">
                                            <div class="thumbnail clearfix">
                                                <figure>
                                                    <a href="#">
                                                        <img src="images/a2.png" alt=""
                                                             class="img-responsive">
                                                    </a>
                                                </figure>
                                                <div class="caption">
                                                    <div class="txt2">Toyota</div>
                                                    <div class="info clearfix">
                                                        <span class="price">AED 18,995</span>
                                                        <span class="speed">249 HP</span>
                                                    </div>
                                                    <div class="txt3">
                                                        2016 • Automatic • Yellow • Diesel
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="thumb2">
                                            <div class="thumbnail clearfix">
                                                <figure>
                                                    <a href="#">
                                                        <img src="images/c3.png" alt=""
                                                             class="img-responsive">
                                                    </a>
                                                </figure>
                                                <div class="caption">
                                                    <div class="txt2">Mitsubishi Pajero</div>
                                                    <div class="info clearfix">
                                                        <span class="price">AED 64,380</span>
                                                        <span class="speed">250 HP</span>
                                                    </div>
                                                    <div class="txt3">
                                                        2017 • Automatic • Green • Diesel
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="thumb2">
                                            <div class="thumbnail clearfix">
                                                <figure>
                                                    <a href="#">
                                                        <img src="images/d2.png" alt=""
                                                             class="img-responsive">
                                                    </a>
                                                </figure>
                                                <div class="caption">
                                                    <div class="txt2">Mitsubishi Pajero</div>
                                                    <div class="info clearfix">
                                                        <span class="price">AED 24,380</span>
                                                        <span class="speed">250 HP</span>
                                                    </div>
                                                    <div class="txt3">
                                                        2015 • Automatic • White • Petrol
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="thumb2">
                                            <div class="thumbnail clearfix">
                                                <figure>
                                                    <a href="#">
                                                        <img src="images/a2.png" alt=""
                                                             class="img-responsive">
                                                    </a>
                                                </figure>
                                                <div class="caption">
                                                    <div class="txt2">Mitsubishi Pajero</div>
                                                    <div class="info clearfix">
                                                        <span class="price">AED 95,900</span>
                                                        <span class="speed">250 HP</span>
                                                    </div>
                                                    <div class="txt3">
                                                        2016 • Manual • Red • Petrol
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="thumb2">
                                            <div class="thumbnail clearfix">
                                                <figure>
                                                    <a href="#">
                                                        <img src="images/d3.png" alt=""
                                                             class="img-responsive">
                                                    </a>
                                                </figure>
                                                <div class="caption">
                                                    <div class="txt2">Mitsubishi Pajero</div>
                                                    <div class="info clearfix">
                                                        <span class="price">AED 98,995</span>
                                                        <span class="speed">250 HP</span>
                                                    </div>
                                                    <div class="txt3">
                                                        2015 • Automatic • Blue • Petrol
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="testimonials">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div id="review">
                        <div class="">
                            <div class="carousel-box">
                                <div class="inner">
                                    <div class="carousel main">
                                        <ul>
                                            <li>
                                                <div class="review">
                                                    <div class="review_inner">
                                                        <div class="testimonial-wrapper">
                                                            <div class="txt1"><b>ELCHIN FARMANLI,</b> Customer, RANGE
                                                                ROVER
                                                                Owner
                                                            </div>
                                                            <div class="txt2">
                                                                <div class="img-wrapper"><img
                                                                            src="images/customer.jpg" alt=""
                                                                            class="img-responsive"></div>
                                                            </div>
                                                            <div class="txt3">Donec facilisis velit eust. Phasellus cons
                                                                quat. Aenean vitae quam. Vivamus et nunc. Nunc
                                                                consequsem
                                                                velde metus imperdiet lacinia. Nam rutrum congue diam.
                                                                Vestibulum acda risus eros auctor egestas. Morbids sem
                                                                magna, viverra quis sollicitudin quis consectetuer quis
                                                                nec magna.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="review">
                                                    <div class="review_inner">
                                                        <div class="testimonial-wrapper">
                                                            <div class="txt1"><b>RASHAD GAFAR,</b> Customer, TIIDA Owner
                                                            </div>
                                                            <div class="txt2">
                                                                <div class="img-wrapper"><img
                                                                            src="images/customer.jpg" alt=""
                                                                            class="img-responsive"></div>
                                                            </div>
                                                            <div class="txt3">Donec facilisis velit eust. Phasellus cons
                                                                quat. Aenean vitae quam. Vivamus et nunc. Nunc
                                                                consequsem
                                                                velde metus imperdiet lacinia. Nam rutrum congue diam.
                                                                Vestibulum acda risus eros auctor egestas. Morbids sem
                                                                magna, viverra quis sollicitudin quis consectetuer quis
                                                                nec magna.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="review">
                                                    <div class="review_inner">
                                                        <div class="testimonial-wrapper">
                                                            <div class="txt1"><b>AHMAD HOMAMAD,</b> Customer, BMW Owner
                                                            </div>
                                                            <div class="txt2">
                                                                <div class="img-wrapper"><img
                                                                            src="images/customer.jpg" alt=""
                                                                            class="img-responsive"></div>
                                                            </div>
                                                            <div class="txt3">Donec facilisis velit eust. Phasellus cons
                                                                quat. Aenean vitae quam. Vivamus et nunc. Nunc
                                                                consequsem
                                                                velde metus imperdiet lacinia. Nam rutrum congue diam.
                                                                Vestibulum acda risus eros auctor egestas. Morbids sem
                                                                magna, viverra quis sollicitudin quis consectetuer quis
                                                                nec magna.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="review_pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include 'modules/suggestions.php'; ?>

    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script>
        $(function () {
            $(window).scroll(function () {
                var top = $(window).scrollTop();
                var bannerIndex = $('.banner_index');
                var bannerIndexContainer = $('.banner_index_fixed_container');
                if (top > 680) {
                    bannerIndex.addClass('banner_index_fixed');
                    bannerIndexContainer.show();
                } else {
                    bannerIndex.removeClass('banner_index_fixed');
                    bannerIndexContainer.hide();
                }
            })
        })
    </script>


<?php include 'modules/footer.php'; ?>