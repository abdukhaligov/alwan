/*============Для мобильного гамбургера P.S. надо дополнить для автоматизации==========*/
function h_humb() {

    /*Открытие гамбургер меню*/
    var speed = 10;
    $('.mob-hamb-button').click(function () {
        $('.mob-hamb').addClass("mob-hamb-active");
    });
    $('.mob-hamb-x').click(function () {
        $('.mob-hamb').removeClass("mob-hamb-active");
        $('div').removeClass("mob-hamb-remove-margin");
        $('div').removeClass("mob-hamb-add-margin");
        $('div').removeClass("mob-hamb-add-margin-x2");
        $('.mob-hamb-se').slideUp(speed);
        $(".mob-hamb-th").slideUp(speed);
    });

    /*Для закрытия можно сделать свайп*/
    $(".mob-hamb").on("swipe", swipeHandler);
    function swipeHandler(event) {
        $('.mob-hamb').removeClass("mob-hamb-active");
        $('div').removeClass("mob-hamb-remove-margin");
        $('div').removeClass("mob-hamb-add-margin");
        $('div').removeClass("mob-hamb-add-margin-x2");
        $('.mob-hamb-se').slideUp(speed);
        $(".mob-hamb-th").slideUp(speed);
    }


    /*Эффект открытия самбеню*/
    $('.mob-hamb-fr_').click(function () {
        if ($(this).parent(".mob-hamb-fr").siblings(".mob-hamb-fr").children(".mob-hamb-se").hasClass("mob-hamb-remove-margin")) {
            $(this).parent(".mob-hamb-fr").siblings(".mob-hamb-fr").children('.mob-hamb-se').slideToggle(300);
            $(this).parent(".mob-hamb-fr").siblings(".mob-hamb-fr").children('.mob-hamb-se').toggleClass("mob-hamb-remove-margin");

            $(this).siblings('.mob-hamb-se').slideToggle(speed);
            $(this).siblings('.mob-hamb-se').toggleClass("mob-hamb-remove-margin");
            $('div').removeClass("mob-hamb-add-margin-x2");
            $(".mob-hamb-th").removeClass("mob-hamb-remove-margin");
            $('.mob-hamb-th').slideUp(speed);

        }
        else {
            $(this).siblings('.mob-hamb-se').slideToggle(speed);
            $(".mob-hamb-fr").toggleClass("mob-hamb-add-margin");
            $(this).siblings('.mob-hamb-se').toggleClass("mob-hamb-remove-margin");

            $(".mob-hamb-th").removeClass("mob-hamb-remove-margin");
            $(".mob-hamb-th").slideUp(speed);
            $(".mob-hamb-fr").removeClass("mob-hamb-add-margin-x2");
        }


    })

    $('.mob-hamb-se_').click(function () {
        if ($(this).parent(".mob-hamb-se").siblings(".mob-hamb-se").children(".mob-hamb-th").hasClass("mob-hamb-remove-margin")) {
            $(this).parent(".mob-hamb-se").siblings(".mob-hamb-se").children('.mob-hamb-th').slideToggle(speed);
            $(this).parent(".mob-hamb-se").siblings(".mob-hamb-se").children('.mob-hamb-th').toggleClass("mob-hamb-remove-margin");
            $(this).siblings('.mob-hamb-th').slideToggle(speed);
            $(this).siblings('.mob-hamb-th').toggleClass("mob-hamb-remove-margin");
        }
        else {
            $(this).siblings('.mob-hamb-th').slideToggle(speed);
            $(".mob-hamb-se").toggleClass("mob-hamb-add-margin-x2");
            $(".mob-hamb-fr").toggleClass("mob-hamb-add-margin-x2");
            $(this).siblings(".mob-hamb-th").toggleClass("mob-hamb-remove-margin");
        }


    })
}

/*=======Для мобильной версии qetquotation в файле car#.php будет выше чем галлерея=======*/
function h_changeqaq() {

    var temp = $(".comment-web").html();
    var temp_ = $(".quotation-web").html();

    if ($(window).width() < 767) {
        $('.comment-web').html(temp);

        $('.quotation-mob').html(temp_);
        $('.comment-mob').html(temp);


    }
    else {

    }
}

/*Меняет цвет текста хэдера при скроллинге вниз*/
function h_head() {
    $(window).scroll(function () {
        var top = $(window).scrollTop();
        var temp = $(".h_header_nav").next('div').height();

        if (top > temp - 50) {
            $(".navbar_  .nav  li   a").addClass("grey");

        } else {
            $(".navbar_  .nav  li   a").removeClass("grey");
        }
    })
}


/*Проверяет на наличие класса ниже, который находится только в файле car#.php*/
function h_cars() {
    if ($('*').is(".h_header_nav")) {
        $('.h_header_nav').html("<link href=\"css/h-style-exclusive.css\" rel=\"stylesheet\">")
    }
}


h_humb();
h_changeqaq();
h_cars();
h_head();