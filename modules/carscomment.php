<div id="tabs3-1">
    <div class="comment-block clearfix">
        <figure><img src="images/customer_sm.jpg" alt="" class="img-responsive"></figure>
        <div class="caption">
            <div class="top clearfix">
                <div class="txt1">Rashad Gafar</div>
                <div class="txt2">30 MAY 2017<a href="#">Posted at 12:25 AM</a></div>
                <div class="ic10"><a href="#"> <i class="fa fa-mail-reply"></i> <span></span> </a></div>
            </div>
            <div class="txt3"> In eget dolor. Lorem ipsum dolor sit amet consectetuer adipiscing elit. Ut ornare cursus
                nibh. Sed lorem volutpat purus scel erisque dapibus. Etiam adipiscing ante nec nibh. In in felis in
                metus mollis blandit.
            </div>
        </div>
    </div>
    <div class="comment-block clearfix left1">
        <figure><img src="images/customer_sm.jpg" alt="" class="img-responsive"></figure>
        <div class="caption">
            <div class="top clearfix">
                <div class="txt1">Elchin Farmanli</div>
                <div class="txt2">30 MAY 2017 <a href="#">Posted at 12:25 AM</a></div>
                <div class="ic10"><a href="#"> <i class="fa fa-mail-reply"></i> <span></span> </a></div>
            </div>
            <div class="txt3"> In eget dolor. Lorem ipsum dolor sit amet consectetuer adipiscing elit. Ut ornare cursus
                nibh. Sed lorem volu tpat purus scelerisque dapibus. Etiam adipiscing ante nec nibh. In in felis in
                metus mollis
            </div>
        </div>
    </div>
    <div class="divider1"></div>
<!--    <div class="title2">ADD A REPLY</div>-->
    <div id="note3"></div>
    <div id="fields3">
        <form id="ajax-contact-form3" class="form-horizontal" action="javascript:;">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="inputName2">Your Name</label>
                                <input type="text" class="form-control" id="inputName2" name="name" value="Full Name"
                                       onBlur="if(this.value=='') this.value='Full Name'"
                                       onFocus="if(this.value =='Full Name' ) this.value=''">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group"><label for="inputEmail">Email</label>
                                <input type="text"
                                       class="form-control"
                                       id="inputEmail"
                                       name="email"
                                       value="E-mail address"
                                       onBlur="if(this.value=='') this.value='E-mail address'"
                                       onFocus="if(this.value =='E-mail address' ) this.value=''">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group"><label for="inputMessage2">Your Message</label>
                        <textarea class="form-control" rows="9" id="inputMessage2" name="content"
                                  onBlur="if(this.value=='') this.value='Message'"
                                  onFocus="if(this.value =='Message' ) this.value=''">Message</textarea></div>
                </div>
            </div>
            <button type="submit" class="btn-default btn-cf-submit3">SEND COMMENTS</button>
        </form>
    </div>
</div>