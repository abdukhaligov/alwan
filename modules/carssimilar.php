<div class="row">
    <div class="col-sm-3">
        <div class="thumb2">
            <div class="thumbnail clearfix">
                <figure>
                    <a href="car1.php">
                        <img src="images/b1.png" alt="" class="img-responsive">
                    </a>
                </figure>
                <div class="caption">
                    <div class="txt1">MODEL 2017</div>
                    <div class="txt2">Toyota</div>
                    <div class="info clearfix">
                        <span class="price">AED 24,380</span>
                    </div>
                    <div class="txt3">
                        2017 • Automatic • White • Petrol
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="thumb2">
            <div class="thumbnail clearfix">
                <figure>
                    <a href="car4.php">
                        <img src="images/d1.png" alt="" class="img-responsive">
                    </a>
                </figure>
                <div class="caption">
                    <div class="txt1">MODEL 2018</div>
                    <div class="txt2">Toyota</div>
                    <div class="info clearfix">
                        <span class="price">AED 95,900</span>
                    </div>
                    <div class="txt3">
                        2018 • Manual • Red • Petrol
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="thumb2">
            <div class="thumbnail clearfix">
                <figure>
                    <a href="car2.php">
                        <img src="images/a1.png" alt="" class="img-responsive">
                    </a>
                </figure>
                <div class="caption">
                    <div class="txt1">MODEL 2017</div>
                    <div class="txt2">Toyota</div>
                    <div class="info clearfix">
                        <span class="price">AED 98,995</span>
                    </div>
                    <div class="txt3">
                        2017 • Automatic • Blue • Petrol
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="thumb2">
            <div class="thumbnail clearfix">
                <figure>
                    <a href="car3.php">
                        <img src="images/c1.png" alt="" class="img-responsive">
                    </a>
                </figure>
                <div class="caption">
                    <div class="txt1">MODEL 2017</div>
                    <div class="txt2">Toyota</div>
                    <div class="info clearfix">
                        <span class="price">AED 31,900</span>
                    </div>
                    <div class="txt3">
                        2017 • Automatic • Dark Red • Petrol
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>