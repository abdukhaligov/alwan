<div class="bot1-wrapper">
    <div class="container">
        <div class="bot1 clearfix">
            <div class="row">
                <div class="col-sm-4">
                    <div class="bot1-title"><span>LATEST NEWS</span></div>
                    <div class="news-block">
                        <div class="news1">
                            <div class="txt1">Duis scelerisque aliquet ante donec
                                libero pede porttitor dacu
                            </div>
                            <div class="txt2"><a href="#">Read More</a></div>
                        </div>
                        <div class="news1">
                            <div class="txt1">Duis scelerisque aliquet ante donec
                                libero pede porttitor dacu
                            </div>
                            <div class="txt2"><a href="#">Read More</a></div>
                        </div>
                        <div class="news1">
                            <div class="txt1">Duis scelerisque aliquet ante donec
                                libero pede porttitor dacu
                            </div>
                            <div class="txt2"><a href="#">Read More</a></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="bot1-title"><span>LATEST AUTOS</span></div>
                    <div class="autos-block">
                        <div class="autos1 clearfix">
                            <figure><img src="images/b1.png" alt="" class="img-responsive">
                            </figure>
                            <div class="caption">
                                <div class="txt1">MITSUBISHI PAJERO</div>
                                <div class="txt2">250 HP</div>
                                <div class="txt3"><a href="#">Read More</a></div>
                            </div>
                        </div>
                        <div class="autos1 clearfix">
                            <figure><img src="images/e1.png" alt="" class="img-responsive">
                            </figure>
                            <div class="caption">
                                <div class="txt1">TOYOTA</div>
                                <div class="txt2">249 HP</div>
                                <div class="txt3"><a href="#">Read More</a></div>
                            </div>
                        </div>
                        <div class="autos1 clearfix">
                            <figure><img src="images/d1.png" alt="" class="img-responsive">
                            </figure>
                            <div class="caption">
                                <div class="txt1">NISSAN PATROL</div>
                                <div class="txt2">250 HP</div>
                                <div class="txt3"><a href="#">Read More</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="bot1-title"><span>CONTACT US</span></div>
                    <div class="address2"><span aria-hidden="true" class="ei icon_pin"></span>2Alwan Cars L.L.C.
                        Auto Market. Dubai, UAE
                    </div>
                    <div class="bot1-map-wrapper">
                        <div class="phone2"><span aria-hidden="true" class="ei icon_phone"></span>Phone: 00971 320
                            9922
                        </div>
                        <div class="fax2"><span aria-hidden="true" class="ei icon_printer"></span>FAX: 00971 320
                            9933
                        </div>
                        <div class="email2"><span aria-hidden="true" class="ei icon_mail"></span>Email: <a href="#">info@alwancars-uae.com</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>