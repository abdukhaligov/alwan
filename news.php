<?php include 'modules/header.php'; ?>

<?php include 'modules/showroommin.php'; ?>

<?php include 'modules/navmap.php'; ?>

    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 column-content">
                    <div class="car-view3-wrapper">
                        <div class="car-view1 clearfix">
                            <figure><img src="images/news1.jpg" alt="" class="img-responsive"></figure>
                            <div class="caption">
                                <div class="top-info clearfix">
                                    <div class="info1">
                                        <div class="txt1">New showroom has been opened</div>
                                    </div>
                                </div>
                                <div class="txt4">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tortor dui, scelerisque
                                    ac nisi sed, rutrum euismod sem. Nunc eu tincidunt nulla. In posuere lorem sit amet
                                    felis placerat, quis hendrerit est rutrum. Phasellus non dui aliquam, eleifend enim
                                    dictum, laoreet nisl. Nam arcu nisi, venenatis nec semper sed, semper eget diam.
                                    Vestibulum id lorem metus. Aliquam felis elit, imperdiet non rutrum ac, euismod ut
                                    diam. Donec iaculis at lorem et placerat. Aenean dictum orci sed lectus vulputate,
                                    ut mattis justo feugiat. Pellentesque sodales urna quis nunc iaculis lobortis. Sed
                                    vel ligula egestas, tristique urna sit amet, rutrum enim. Fusce non dignissim
                                    tellus, sed commodo nulla.
                                </div>
                                <div class="bot-info clearfix">
                                    <div class="info3">
                                        <div class="txt6">22/09/2017</div>
                                    </div>
                                    <div class="info4">
                                        <div class="txt7">
                                            <a href="newsarticle.php" class="btn-default btn3">VIEW DETAILS</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="car-view3-wrapper">
                        <div class="car-view1 clearfix">
                            <figure><img src="images/news2.jpg" alt="" class="img-responsive"></figure>
                            <div class="caption">
                                <div class="top-info clearfix">
                                    <div class="info1">
                                        <div class="txt1">Will tax reform lead to more car sales?</div>
                                    </div>
                                </div>
                                <div class="txt4">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tortor dui, scelerisque
                                    ac nisi sed, rutrum euismod sem. Nunc eu tincidunt nulla. In posuere lorem sit amet
                                    felis placerat, quis hendrerit est rutrum. Phasellus non dui aliquam, eleifend enim
                                    dictum, laoreet nisl. Nam arcu nisi, venenatis nec semper sed, semper eget diam.
                                    Vestibulum id lorem metus. Aliquam felis elit, imperdiet non rutrum ac, euismod ut
                                    diam. Donec iaculis at lorem et placerat. Aenean dictum orci sed lectus vulputate,
                                    ut mattis justo feugiat.
                                </div>
                                <div class="bot-info clearfix">
                                    <div class="info3">
                                        <div class="txt6">22/09/2017</div>
                                    </div>
                                    <div class="info4">
                                        <div class="txt7">
                                            <a href="newsarticle.php" class="btn-default btn3">VIEW DETAILS</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="car-view3-wrapper">
                        <div class="car-view1 clearfix">
                            <figure><img src="images/news3.JPG" alt="" class="img-responsive"></figure>
                            <div class="caption">
                                <div class="top-info clearfix">
                                    <div class="info1">
                                        <div class="txt1">Why diesel won't die
                                        </div>
                                    </div>
                                </div>
                                <div class="txt4">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tortor dui, scelerisque
                                    ac nisi sed, rutrum euismod sem. Nunc eu tincidunt nulla. In posuere lorem sit amet
                                    felis placerat, quis hendrerit est rutrum. Phasellus non dui aliquam, eleifend enim
                                    dictum, laoreet nisl. Nam arcu nisi, venenatis nec semper sed, semper eget diam.
                                    Vestibulum id lorem metus. Aliquam felis elit, imperdiet non rutrum ac, euismod ut
                                    diam. Donec iaculis at lorem et placerat. Aenean dictum orci sed lectus vulputate,
                                    ut mattis justo feugiat. Pellentesque sodales urna quis nunc iaculis lobortis. Sed
                                    vel ligula egestas, tristique urna sit amet, rutrum enim. Fusce non dignissim
                                    tellus, sed commodo nulla.
                                </div>
                                <div class="bot-info clearfix">
                                    <div class="info3">
                                        <div class="txt6">22/09/2017</div>
                                    </div>
                                    <div class="info4">
                                        <div class="txt7">
                                            <a href="newsarticle.php" class="btn-default btn3">VIEW DETAILS</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="car-view3-wrapper">
                        <div class="car-view1 clearfix">
                            <figure><img src="images/news4.jpg" alt="" class="img-responsive"></figure>
                            <div class="caption">
                                <div class="top-info clearfix">
                                    <div class="info1">
                                        <div class="txt1">Mercedes bills latest A class as 'smartphone on wheels'</div>
                                    </div>
                                </div>
                                <div class="txt4">
                                    Phasellus non dui aliquam, eleifend enim
                                    dictum, laoreet nisl. Nam arcu nisi, venenatis nec semper sed, semper eget diam.
                                    Vestibulum id lorem metus. Aliquam felis elit, imperdiet non rutrum ac, euismod ut
                                    diam. Donec iaculis at lorem et placerat. Aenean dictum orci sed lectus vulputate,
                                    ut mattis justo feugiat. Pellentesque sodales urna quis nunc iaculis lobortis. Sed
                                    vel ligula egestas, tristique urna sit amet, rutrum enim. Fusce non dignissim
                                    tellus, sed commodo nulla. Nulla posuere, nunc ac ultrices pretium, mi quam
                                    dignissim magna, vel luctus magna sapien et velit. Quisque in felis odio.
                                </div>
                                <div class="bot-info clearfix">
                                    <div class="info3">
                                        <div class="txt6">22/09/2017</div>
                                    </div>
                                    <div class="info4">
                                        <div class="txt7">
                                            <a href="newsarticle.php" class="btn-default btn3">VIEW DETAILS</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="car-view3-wrapper">
                        <div class="car-view1 clearfix">
                            <figure><img src="images/news5.jpeg" alt="" class="img-responsive"></figure>
                            <div class="caption">
                                <div class="top-info clearfix">
                                    <div class="info1">
                                        <div class="txt1">Aston Martin seeks EV partner in China</div>
                                    </div>
                                </div>
                                <div class="txt4">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tortor dui, scelerisque
                                    ac nisi sed, rutrum euismod sem. Nunc eu tincidunt nulla. In posuere lorem sit amet
                                    felis placerat, quis hendrerit est rutrum. Phasellus non dui aliquam, eleifend enim
                                    dictum, laoreet nisl. Pellentesque sodales urna quis nunc iaculis lobortis. Sed
                                    vel ligula egestas, tristique urna sit amet, rutrum enim. Fusce non dignissim
                                    tellus, sed commodo nulla. Nulla posuere, nunc ac ultrices pretium, mi quam
                                    dignissim magna, vel luctus magna sapien et velit. Quisque in felis odio.
                                </div>
                                <div class="bot-info clearfix">
                                    <div class="info3">
                                        <div class="txt6">22/09/2017</div>
                                    </div>
                                    <div class="info4">
                                        <div class="txt7">
                                            <a href="newsarticle.php" class="btn-default btn3">VIEW DETAILS</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="car-view3-wrapper">
                        <div class="car-view1 clearfix">
                            <figure><img src="images/news6.jpg" alt="" class="img-responsive"></figure>
                            <div class="caption">
                                <div class="top-info clearfix">
                                    <div class="info1">
                                        <div class="txt1">Aptiv reports $256 million in Q4 net income following Delphi
                                            spinoff
                                        </div>
                                    </div>
                                </div>
                                <div class="txt4">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tortor dui, scelerisque
                                    ac nisi sed, rutrum euismod sem. Nunc eu tincidunt nulla. In posuere lorem sit amet
                                    felis placerat, quis hendrerit est rutrum. Phasellus non dui aliquam, eleifend enim
                                    dictum, laoreet nisl. Nam arcu nisi, venenatis nec semper sed, semper eget diam.
                                    Vestibulum id lorem metus. Aliquam felis elit, imperdiet non rutrum ac, euismod ut
                                    diam. Donec iaculis at lorem et placerat. Aenean dictum orci sed lectus vulputate,
                                    ut mattis justo feugiat. Pellentesque sodales urna quis nunc iaculis lobortis.
                                    <div class="bot-info clearfix">
                                        <div class="info3">
                                            <div class="txt6">22/09/2017</div>
                                        </div>
                                        <div class="info4">
                                            <div class="txt7">
                                                <a href="newsarticle.php" class="btn-default btn3">VIEW DETAILS</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pager_wrapper">
                            <ul class="pager clearfix">
                                <li class="prev"><a href="#"></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li class="li"><a href="#">2</a></li>
                                <li class="li"><a href="#">3</a></li>
                                <li class="li"><a href="#">4</a></li>
                                <li class="dots">....</li>
                                <li class="next"><a href="#"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include 'modules/suggestions.php'; ?>


<?php include 'modules/footer.php'; ?>