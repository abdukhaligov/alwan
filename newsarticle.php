<?php include 'modules/header.php'; ?>

<?php include 'modules/showroommin.php'; ?>

<?php include 'modules/navmap.php'; ?>

    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 column-content">
                    <div class="gslider-wrapper">
                        <div id="gslider" class="flexslider">
                            <ul class="slides">
                                <li>
                                    <img src="images/news.jpg" alt="" class="img-responsive">
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div>
                        <div class="title2">
                            <p>NEWS TITLE</p>
                        </div>
                        <p>
                            Quisque at velit finibus, vestibulum ex vitae, maximus elit. Suspendisse vel urna vitae
                            metus laoreet ultrices. Aenean euismod varius elementum. Vestibulum ante ipsum primis in
                            faucibus orci luctus et ultrices posuere cubilia Curae; Proin consequat, nisi non imperdiet
                            luctus, nunc dolor euismod est, nec facilisis odio leo eu tortor. Proin efficitur, enim in
                            lacinia aliquet, ex dolor ornare sem, at egestas odio lacus non dolor. Duis dictum dolor
                            eget nibh euismod tristique. Vestibulum pharetra lobortis ipsum a pretium. Quisque nisi
                            mauris, fermentum sed lectus a, tincidunt fringilla libero. Proin auctor sem sit amet mi
                            facilisis rutrum. In congue lectus sem, a hendrerit erat finibus sed. Fusce lorem sem,
                            iaculis et nisi ac, imperdiet dignissim mauris. Duis tempor aliquam ex, eget posuere turpis
                            facilisis eget. Nulla efficitur ligula risus, ut euismod metus pulvinar quis.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php include 'modules/suggestions.php'; ?>


<?php include 'modules/footer.php'; ?>