<?php include 'modules/header.php'; ?>

    <div class="pages">
        <main class="page page-5 p5-main">
            <section class="cont-pw">
                <div class="p3-div1">
                    <a href="products.php">
                    <button class="p3-bt1 go-btn" data-type="main" data-loc="1">
                        <span class="p3-sp1">
                        <
                        </span>
                        <span class="p3-sp2">
                        SUV & MPV AC
                        </span>
                    </button>
                    </a>
                    <a href="models.php">
                    <button class="p3-bt1 go-btn" data-type="sub" data-loc="1">
                        <span class="p3-sp1">
                        <
                        </span>
                        <span class="p3-sp2">
                        TOYOTA
                        </span>
                    </button>
                    </a>
                    <a href="cars.php">
                    <button class="p3-bt1 go-btn" data-type="brand" data-loc="1">
                        <span class="p3-sp1">
                        <
                        </span>
                        <span class="p3-sp2">
                        PATROL ARMADA
                        </span>
                    </button>
                    </a>
                </div>
                <div class="p5-div5">
                    <div class="p5-div6">
                        <div class=" p5-div7 swiper-container">
                            <div class="swiper-wrapper">
                                <style type="text/css">
                                    @media (max-width: 1350px) {
                                        .swiper-slide img {
                                            width: 720px;
                                        }
                                    }

                                    @media (max-width: 991px) {
                                        .swiper-slide img {
                                            width: 310px;
                                        }

                                        .p5-div7 {
                                            height: 215px;
                                        }
                                    }

                                    @media (max-width: 450px) {
                                        .swiper-slide img {
                                            width: 310px;
                                        }
                                    }
                                </style>
                                <div class="swiper-slide"><img src="assets/css/img/p1.png"></div>
                                <div class="swiper-slide"><img src="assets/css/img/p2.png"></div>
                                <div class="swiper-slide"><img src="assets/css/img/p3.png"></div>
                                <div class="swiper-slide"><img src="assets/css/img/p4.png"></div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                    <div class="p5-div9">
                        <div class="p5-div8">
                        </div>
                        <div class="p5-div10">
                            Patrol Armada
                        </div>
                        <div class="p5-div11">
                            Model: 2018
                        </div>
                        <div class="p5-div12">
                            price
                        </div>
                        <div class="p5-div13">
                            AED
                            <span class="p5-sp1">
                                320 000
                                </span>
                        </div>
                        <style type="text/css">
                            .scrollbar {
                                margin-left: 30px;
                                float: left;
                                height: 300px;
                                width: 65px;
                                background: #F5F5F5;
                                overflow-y: scroll;
                                margin-bottom: 25px;
                            }

                            .force-overflow {
                                padding-top: 0px !important;
                                margin: 0px !important;
                            }

                            #style-1::-webkit-scrollbar-track {
                                -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
                                border-radius: 10px;
                                background-color: #F5F5F5;
                            }

                            #style-1::-webkit-scrollbar {
                                width: 5px;
                                background-color: #F5F5F5;
                            }

                            #style-1::-webkit-scrollbar-thumb {
                                border-radius: 20px;
                                -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
                                background-color: #555;
                            }

                            /*
                            *  STYLE 2
                            */
                        </style>
                        <div class="p5-div14" id="style-1">
                            <div class="force-overflow"></div>
                            <div>
                                    <span class="p5-sp2">
                                    Description
                                    </span>
                                Patrol Le VK56 V8 7AT PTNM P18
                            </div>
                            <div>
                                    <span class="p5-sp2">
                                    Body Type
                                    </span>
                                SUV
                            </div>
                            <div>
                                    <span class="p5-sp2">
                                    Exterior Dimensions (L x W x H)(m m)
                                    </span>
                                5140x1995x1940
                            </div>
                            <div>
                                    <span class="p5-sp2">
                                    Engine
                                    </span>
                                VK 50 VD
                            </div>
                            <div>
                                    <span class="p5-sp2">
                                    Max Torque kq.m/rpm
                                    </span>
                                7 speed AT
                            </div>
                            <div>
                                    <span class="p5-sp2">
                                    57.1@4000
                                    </span>
                                ECCS Electronic fuel injection control Hight Octane Gasoline
                            </div>
                            <div>
                                    <span class="p5-sp2">
                                    Max Power hp/rpm
                                    </span>
                                400 @5800
                            </div>
                            <div>
                                    <span class="p5-sp2">
                                    Displacement cc
                                    </span>
                                552
                            </div>
                            <div>
                                    <span class="p5-sp2">
                                    Fuel System
                                    </span>
                                SPEED Response TOPS Streening
                            </div>
                            <div>
                                    <span class="p5-sp2">
                                    Transmission
                                    </span>
                                100+40
                            </div>
                            <div>
                                    <span class="p5-sp2">
                                    Displacement cc
                                    </span>
                                552
                            </div>
                            <div>
                                    <span class="p5-sp2">
                                    Power:
                                    </span>
                                141 bhp@4000 rpm
                            </div>
                            <div>
                                    <span class="p5-sp2">
                                    Color:
                                    </span>
                                WHITE
                            </div>
                        </div>
                        <div class="p5-div15">
                            <img src="assets/css/img/png18.png" class="p5-img2">
                        </div>
                    </div>
                </div>
                <div class="p5-div16 p3-div2 drag-block p5-div666 ">
                    <!-- <button class="p5-btn1">
                        GET A QUOTE
                        <img src="assets/css/img/png19.png" class="p5-img3">
                        </button>

                        <button class="p5-btn1">
                        SIMILIAR VEHICLES

                        </button> -->
                    <!--    class : active   ---  background : yellow    -->
                    <a class="p3-div3 go-btn" data-type="model" href="">
                        <div class="p3-div4">
                            <img class="p3-img1" src="assets/css/img/cars/0.jpg">
                            <div class="p4-div1">
                                PATROL ARMADA
                            </div>
                        </div>
                    </a>
                    <a class="p3-div3 go-btn" data-type="model" href="">
                        <div class="p3-div4">
                            <img class="p3-img1" src="assets/css/img/cars/2.jpg">
                            <div class="p4-div1">
                                PATROL ARMADA
                            </div>
                        </div>
                    </a>
                    <a class="p3-div3 go-btn" data-type="model" href="">
                        <div class="p3-div4">
                            <img class="p3-img1" src="assets/css/img/cars/3.jpg">
                            <div class="p4-div1">
                                PATROL ARMADA
                            </div>
                        </div>
                    </a>
                    <a class="p3-div3 go-btn" data-type="model" href="">
                        <div class="p3-div4">
                            <img class="p3-img1" src="assets/css/img/cars/4.jpg">
                            <div class="p4-div1">
                                PATROL ARMADA
                            </div>
                        </div>
                    </a>
                    <a class="p3-div3 go-btn" data-type="model" href="">
                        <div class="p3-div4">
                            <img class="p3-img1" src="assets/css/img/cars/5.jpg">
                            <div class="p4-div1">
                                PATROL ARMADA
                            </div>
                        </div>
                    </a>
                </div>
            </section>
        </main>
    </div>

    <!-- Swiper JS -->
    <script src="assets/js/swiper.min.js"></script>
    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper('.swiper-container', {
            pagination: {
                el: '.swiper-pagination',

            },
            slidesPerView: 1,
            spaceBetween: 30,
            loop: true,

            centeredSlides: true,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },

        });
    </script>

<?php include 'modules/footer.php'; ?>