<!DOCTYPE html>
<html>
<head>
    <title>Alwan - showroom</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/showroom.css">
    <link rel="stylesheet" href="assets/css/override.css">
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/siema.min.js"></script>
    <link rel="stylesheet" href="assets/css/swiper.min.css">
    <script>
        $(window).on('load', app.load.bind(app));
        $(window).resize(app.resize.bind(app));
    </script>
</head>
<body>
<header>
    <div class="cont-pw">
        <a href="../showroom" class="logo-btn header_logo">
            <img style="width: 328px;" src="assets/css/img/Logo.png">
        </a>
        <nav class="header-nav ">
        </nav>
    </div>
</header>
<div class="pages">
    <style>
        .swiper-container {
            width: 100%;
            height: 100%;
        }

        .swiper-slide {
            text-align: center;
            /* Center slide text vertically */
            display: -webkit-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
        }
    </style>
    <main class="page page-4 p4-main">
        <section class="cont-pw">
            <div class="p3-div1">
                <a href="products.php">
                <button class="p3-bt1 go-btn" data-type="main" data-loc="1">
                        <span class="p3-sp1">
                        <
                        </span>
                    <span class="p3-sp2">
                        SUV & MPV AC
                        </span>
                </button>
                </a>
                <a href="models.php">
                <button class="p3-bt1 go-btn" data-type="sub" data-loc="1">
                        <span class="p3-sp1">
                        <
                        </span>
                    <span class="p3-sp2">
                        TOYOTA
                        </span>
                </button>
                </a>
            </div>
            <div class="siema full-width">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="p3-div2 drag-block">
                                <!--    class : active   ---  background : yellow    -->
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/0.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/2.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/3.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/4.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/5.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/6.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/0.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/2.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/3.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/4.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/5.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/6.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/0.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/2.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/3.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/4.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/5.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/6.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="p3-div2 drag-block">
                                <!--    class : active   ---  background : yellow    -->
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/0.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/2.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/3.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/4.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/5.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/6.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/0.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/2.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/3.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/4.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/5.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/6.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/0.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/2.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/3.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/4.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/5.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/6.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="p3-div2 drag-block">
                                <!--    class : active   ---  background : yellow    -->
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/0.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/2.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/3.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/4.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/5.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/6.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/0.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/2.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/3.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/4.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/5.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/6.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/0.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/2.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/3.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/4.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/5.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                                <a class="p3-div3 go-btn" data-type="model" href=car.php>
                                    <div class="p3-div4">
                                        <img class="p3-img1" src="assets/css/img/cars/6.jpg">
                                        <div class="p4-div1">
                                            PATROL ARMADA
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                </div>
                <!-- Swiper JS -->
                <script src="assets/js/swiper.min.js"></script>
                <!-- Initialize Swiper -->
                <script>
                    var swiper = new Swiper('.swiper-container', {
                        pagination: {
                            el: '.swiper-pagination',

                        },
                        slidesPerView: 1,
                        spaceBetween: 30,
                        loop: true,
                    });
                </script>
            </div>
        </section>
    </main>
</div>
<footer>
    <div class="cont-pw">
        © 2017 ALWAN CARS / Powered by Weblizard.ae
    </div>
</footer>
<script></script>
</body>
</html>