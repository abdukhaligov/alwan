<?php include 'modules/header.php'; ?>
    <div class="pages">
        <main class="page page-1 p1-main">
            <a href="products.php">
                <section id="o_first" class="p1-2-sec1 p1-sec1 active go-btn" data-type="other" data-loc="1">
                    <div class="p1-2-dv1">
                    </div>
                    <div class="cont-pw p1-dv1 font24b">
                        <div class="p1-2-dv1">
                        </div>
                        OUR CARS
                    </div>
                </section>

            </a>
            <div>
                <a href="../index.php">
                    <section id="i_second" class="p1-2-sec1 p1-sec1 active go-btn main_menu" data-type="web"
                             data-loc="1">
                        <div class="cont-pw p1-dv1 font24b">
                            MAIN WEBSITE
                        </div>
                    </section>
                </a>
                <a href="../about.php">
                    <section id="i_third" class="p1-2-sec1 p1-sec1 active go-btn main_menu" data-type="about"
                             data-loc="1">
                        <div class="cont-pw p1-dv1 font24b">
                            ABOUT
                        </div>
                    </section>
                </a>
                <a href="../news.php">
                    <section id="i_fourth" class="p1-2-sec1 p1-sec1 active go-btn main_menu" data-type="news"
                             data-loc="1">
                        <div class="cont-pw p1-dv1 font24b">
                            NEWS
                        </div>
                    </section>
                </a>
                <a href="../news.php">
                    <section id="i_fifth" class="p1-2-sec1 p1-sec1 active go-btn main_menu" data-type="career"
                             data-loc="1">
                        <div class="cont-pw p1-dv1 font24b">
                            CAREER
                        </div>
                    </section>
                </a>
                <a href="../contact.php">
                    <section id="i_sixth" class="p1-2-sec1 p1-sec1 active go-btn main_menu" data-type="contact"
                             data-loc="1">
                        <div class="cont-pw p1-dv1 font24b">
                            CONTACT US
                        </div>
                    </section>
                </a>
            </div>
        </main>
        <style type="text/css">
            .h_before {
                background-color: red;
                width: 100%;
                height: 200px;
                transition: all 2000ms;
            }

            .h_after {
                background-color: green;
                transition: all 2000ms;
            }
        </style>
    </div>
<?php include 'modules/footer.php'; ?>