<?php include 'modules/header.php'; ?>

    <div class="pages">
        <main class="page page-2 p2-main">
            <!--   section class=active :   img + color --- active    -->
            <a href="models.php">
                <section id="o_first" class="p1-2-sec1 p2-sec1 active go-btn" data-type="sub" data-loc="1">
                    <div class="p1-2-dv1">
                    </div>
                    <div class="cont-pw p2-dv1 font24b">
                        <div class="p1-2-dv1">
                        </div>
                        ALL VEHICLES
                    </div>
                </section>
            </a>
            <a href="models.php">
                <section id="o_second" class="p1-2-sec1 p2-sec2 go-btn" data-type="sub" data-loc="1">
                    <div class="p1-2-dv1">
                    </div>
                    <div class="cont-pw p2-dv2 font24b">
                        <div class="p1-2-dv1">
                        </div>
                        CARS
                    </div>
                </section>
            </a>
            <a href="models.php">
                <section id="o_third" class="p1-2-sec1 p2-sec3 go-btn" data-type="sub" data-loc="1">
                    <div class="p1-2-dv1">
                    </div>
                    <div class="cont-pw p2-dv3 font24b">
                        <div class="p1-2-dv1">
                        </div>
                        SUV & MPV AC
                    </div>
                </section>
            </a>
            <a href="models.php">
                <section id="o_fourth" class="p1-2-sec1 p2-sec4 go-btn" data-type="sub" data-loc="1">
                    <div class="p1-2-dv1">
                    </div>
                    <div class="cont-pw p2-dv4 font24b">
                        <div class="p1-2-dv1">
                        </div>
                        COMMERCIAL CEHICLES
                    </div>
                </section>
            </a>
        </main>
    </div>
<?php include 'modules/footer.php'; ?>